# 客户端启动游戏主图片

## 对于开发者：

大家可以自行创建自己的分支然后放上自己要提交的图片，图片名字请保持为**index.jpg**，图片比例为150*84，大小不要超过1M

提交合并请求后，对应管理会审核

## 对于管理：

检查图片是否合乎要求，名字需要为index.jpg，不要覆盖本文件（README.md）。通过就可以merge，merge后客户端将自动获取到新的图片。